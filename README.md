# hobby101

### First create .env file from sameple
```
$ cp .env.sample .env
```

### Install Lib
```
$ yarn intsall
```

### Start With Node
```
$ yarn start
```

### Start With NodeMon
```
$ yarn start:dev
```

### Start With Docker
```
$ docker compose up --build --remove-orphans
```


### ESLint
```
$ yarn lint
```

### Unit test
```
$ yarn test
```

### Coverage 100%
```
$ yarn cover
```

## Open Landing
```
http://localhost:3000
```


## Open Document (Swagger)
```
http://localhost:3000/docs
```
