﻿module.exports = {
  server: {
    port: process.env.PORT || 8000,
    host: process.env.HOST || 'localhost'
  },
  github: {
    api: {
      url: 'https://api.github.com',
      path: {
        repositorySearch: '/search/repositories'
      }
    },
    pageSize: 10,
    userAgent: 'NodeJS'
  }
}
