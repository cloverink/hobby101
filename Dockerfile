FROM node:14.18-alpine

RUN mkdir -p /usr/src/app && chmod -R g+rw /usr/src/app

WORKDIR /usr/src/app

COPY package.json yarn.lock /usr/src/app/

RUN yarn install

COPY . .

EXPOSE 3000

CMD ["yarn", "start"]
