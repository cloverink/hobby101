﻿const Hapi = require('@hapi/hapi')
const Inert = require('@hapi/inert')
const Vision = require('@hapi/vision')
const HapiSwagger = require('hapi-swagger')
const Joi = require('joi')
const Ejs = require('ejs')

const config = require('./config')

const { generateGroupChild } = require('./services/groupchild')
const { generateGithubRepositorySearch } = require('./services/github')

const init = async() => {

  const server = Hapi.server(config.server)

  const routerLanding = [
    {
      method: 'GET',
      path: '/',
      handler: (request, h) => {
        return h.file('./public/index.html')
      }
    },
    {
      method: 'GET',
      path: '/groupchild',
      handler: (request, h) => {
        return h.file('./public/group-child.html')
      }
    },
    {
      method: 'GET',
      path: '/github',
      handler: async(req, h) => {
        const params = req.query || {}
        const { q, page, per_page, total_count, items } = await generateGithubRepositorySearch(params.q, params.page)
        const from = (per_page * (page - 1)) + 1
        const to = page * per_page

        const prev = (page - 1 <= 0) ? 1 : page - 1
        const next = page + 1

        return h.view('github-search', {
          from,
          to,
          prev,
          next,
          q,
          page,
          per_page,
          total_count,
          items
        })
      }
    }
  ]

  const routerAPI = [
    {
      method: 'POST',
      path: '/api/groupchild',
      options: {
        handler: (req, h) => {
          return generateGroupChild(req.payload)
        },
        notes: ['Each child should be placed in the children array of its parent (as defined by the parent_id).'],
        tags: ['api', 'Group Child'],
        validate: {
          payload: Joi.object()
        }
      }
    },
    {
      method: 'GET',
      path: '/api/github/search/repositories',
      options: {
        handler: async(req, h) => {
          const params = req.query || {}
          return await generateGithubRepositorySearch(params.q, params.page)
        },
        notes: ['GitHub Search API with pagination'],
        tags: ['api', 'GitHub Search API'],
        plugins: {
          'hapi-swagger': {
            payloadType: 'form'
          }
        },
        validate: {
          query: Joi.object({
            q: Joi.string().required().description('Search value'),
            page: Joi.number().description('Page Index'),
          }),
        }
      }
    }
  ]

  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: {
        info: {
          title: 'Hobby NodeJS'
        },
        documentationPath: '/docs',
        grouping: 'tags'
      }
    }
  ])

  server.views({
    engines: { ejs: Ejs },
    relativeTo: __dirname,
    path: 'public'
  })

  server.route(
    [
      ...routerLanding,
      ...routerAPI
    ]
  )

  await server.start()
  console.log('Server running on %s', server.info.uri)
}

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})

init()
