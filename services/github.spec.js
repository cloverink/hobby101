﻿const chai = require('chai')
const sinonChai = require('sinon-chai')
const expect = chai.expect

const nock = require('nock')
const { generateGithubRepositorySearch } = require('./github')

chai.use(sinonChai)

describe('services', () => {

  before(() => {
    console.error = () => {}
  })

  beforeEach(() => {
    nock.cleanAll()
  })

  describe('groupchild', () => {
    context('.generateGithubRepositorySearch', () => {
      it('call to github service to get a reposity list by q', async() => {
        const nockGithubSearchService = nock('https://api.github.com')
          .get('/search/repositories')
          .query({
            q: 'nodejs',
            page: 1,
            per_page: 10
          })
          .reply(200, {
            total_count: 1,
            items: [
              {
                id: 1,
                full_name: 'project-1',
                html_url: 'http://example.com',
                description: 'some description',
                owner: {
                  login: 'user-1',
                  html_url: 'http://example.com/user-1'
                }
              }
            ]
          })

        const output = await generateGithubRepositorySearch('nodejs')

        expect(nockGithubSearchService.isDone()).to.be.true
        expect(output).to.be.deep.eq({
          q: 'nodejs',
          page: 1,
          per_page: 10,
          total_count: 1,
          items: [
            {
              id: 1,
              name: 'project-1',
              url: 'http://example.com',
              description: 'some description',
              owner_name: 'user-1',
              owner_url: 'http://example.com/user-1'
            }
          ]
        })
      })

      it('returns empty object when search value is empty', async() => {
        const nockGithubSearchService = nock('https://api.github.com')
          .get('/search/repositories')
          .query({
            q: 'nodejs',
            page: 1,
            per_page: 10
          })
          .reply(200, { foo: 'bar '})

        const output = await generateGithubRepositorySearch()
        expect(nockGithubSearchService.isDone()).to.be.false
        expect(output).to.be.deep.eq({
          q: '',
          page: 1,
          per_page: 10,
          total_count: 0,
          items: []
        })
      })

      it('returns items list as empty array by default', async() => {
        const nockGithubSearchService = nock('https://api.github.com')
          .get('/search/repositories')
          .query({
            q: 'nodejs',
            page: 1,
            per_page: 10
          })
          .reply(200, {
            total_count: 1,
            items: null
          })

        const output = await generateGithubRepositorySearch('nodejs')

        expect(nockGithubSearchService.isDone()).to.be.true
        expect(output).to.be.deep.eq({
          q: 'nodejs',
          page: 1,
          per_page: 10,
          total_count: 1,
          items: []
        })
      })

      it('handle an exception and return empty object insteade', async() => {
        nock('https://api.github.com')
          .get('/search/repositories')
          .query({
            q: 'nodejs',
            page: 1,
            per_page: 10
          })
          .reply(500)

        const output = await generateGithubRepositorySearch('nodejs')
        expect(output).to.be.deep.eq({
          q: '',
          page: 1,
          per_page: 10,
          total_count: 0,
          items: []
        })
      })
    })
  })
})
