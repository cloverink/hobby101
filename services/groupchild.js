﻿const flatObject = input => Object.values(input).flat()
const groupChild = (left, right) => {
  if (!right.parent_id) return [...left, right]
  const p = left.find(l => l.id === right.parent_id)
  if (p) {
    p.children.push(right)
    return left
  }
  return left.map(l => {
    return {
      ...l,
      children: groupChild(l.children, right)
    }
  })
}

const generateGroupChild = input => {
  if (!input) return {}
  return flatObject(input).reduce(groupChild, [])
}

module.exports = {
  flatObject,
  groupChild,
  generateGroupChild
}
