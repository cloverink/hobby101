﻿const request = require('request-promise')
const config = require('../config')

const generateGithubRepositorySearch = async(q, page = 1, pageSize = config.github.pageSize) => {

  const defaultPayload = {
    q: '',
    page: page,
    per_page: pageSize,
    total_count: 0,
    items: []
  }

  try {
    if (!q) return defaultPayload
    const options = {
      uri: `${config.github.api.url}${config.github.api.path.repositorySearch}`,
      method: 'GET',
      headers: {
        'User-Agent': 'NodeJS'
      },
      qs: {
        q: q,
        page: page,
        per_page: pageSize
      },
      json: true
    }

    const githubRepositoryList = await request(options)
    const items = githubRepositoryList.items || []
    return {
      q: q,
      page: +page,
      per_page: pageSize,
      total_count: githubRepositoryList.total_count,
      items: items.map(o => ({
        id: o.id,
        name: o.full_name,
        url: o.html_url,
        description: o.description,
        owner_name: o.owner.login,
        owner_url: o.owner.html_url
      }))
    }
  } catch (e) {
    console.error(e.error)
    return defaultPayload
  }
}

module.exports = {
  generateGithubRepositorySearch
}
