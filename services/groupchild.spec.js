﻿const chai = require('chai')
const sinonChai = require('sinon-chai')
const expect = chai.expect

chai.use(sinonChai)

const { flatObject, generateGroupChild, groupChild } = require('./groupchild')

const fakeInputData = require('../data/input.json')
const fakeOutputData = require('../data/output.json')

describe('services', () => {
  describe('groupchild', () => {
    context('.flatObject', () => {
      it('returns a flatten array from javascript object', () => {

        const input = {
          '0': [{ id: 10 }],
          '1': [{ id: 12 }, { id: 13 }]
        }

        const output = flatObject(input)

        expect(output).to.be.deep.eq([
          { id: 10 },
          { id: 12 },
          { id: 13 },
        ])
      })
    })

    context('.groupChild', () => {
      it('pushed right to left when parent_id is null', () => {
        const left = []
        const right = {
          id: 10,
          title: 'House',
          level: 0,
          children: [],
          parent_id: null
        }

        const output = groupChild(left, right)
        expect(output).to.be.deep.eq([
          {
            id: 10,
            title: 'House',
            level: 0,
            children: [],
            parent_id: null
          }
        ])
      })

      it('pushed child to parent when parent_id matched to id', () => {
        const left = [
          {
            id: 10,
            title: 'House',
            level: 0,
            children: [],
            parent_id: null
          }
        ]
        const right = {
          id: 12,
          title: 'Red Roof',
          level: 1,
          children: [],
          parent_id: 10
        }

        const output = groupChild(left, right)
        expect(output).to.be.deep.eq([
          {
            id: 10,
            title: 'House',
            level: 0,
            children: [
              {
                id: 12,
                title: 'Red Roof',
                level: 1,
                children: [],
                parent_id: 10
              }
            ],
            parent_id: null
          }
        ])
      })

      it('will find a parent from each of children', () => {
        const left = [
          {
            id: 10,
            title: 'House',
            level: 0,
            children: [
              {
                id: 12,
                title: 'Red Roof',
                level: 1,
                children: [],
                parent_id: 10
              }
            ],
            parent_id: null
          }
        ]

        const right = {
          id: 17,
          title: 'Blue Window',
          level: 2,
          children: [],
          parent_id: 12
        }

        const output = groupChild(left, right)
        expect(output).to.be.deep.eq([
          {
            id: 10,
            title: 'House',
            level: 0,
            children: [
              {
                id: 12,
                title: 'Red Roof',
                level: 1,
                children: [
                  {
                    id: 17,
                    title: 'Blue Window',
                    level: 2,
                    children: [],
                    parent_id: 12
                  }
                ],
                parent_id: 10
              }
            ],
            parent_id: null
          }
        ])
      })

    })

    context('.generateGroupChild', () => {
      it('returns an empty object when input is null or undefined', () => {
        expect(generateGroupChild(null)).to.be.deep.eq({})
        expect(generateGroupChild(undefined)).to.be.deep.eq({})
      })

      it('returns each child should be placed in the children array of its parent', () => {
        const output = generateGroupChild(fakeInputData)
        expect(output).to.be.deep.eq(fakeOutputData)
      })
    })
  })
})
